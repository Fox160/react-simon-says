import GameInfo from "./components/GameInfo";
import Board from "./components/Board";

import style from './App.module.scss';

const App = () => {
  return (
    <div className={style.container}>
      <h1>Simon Says</h1>

      <div className={style.content}>
        <Board />
        <GameInfo />
      </div>
    </div>
  );
}

export default App;
