import { createSlice } from "@reduxjs/toolkit";
import reducers from "./reducers";

const initialState = [
  {
    id: "green",
    active: false,
  },
  {
    id: "red",
    active: false,
  },
  {
    id: "yellow",
    active: false,
  },
  {
    id: "blue",
    active: false,
  }
];

const { actions, reducer } = createSlice({
  name: 'pads',
  initialState,
  reducers,
});

export const { lightenPad, lightenOffPad } = actions;

export default reducer;