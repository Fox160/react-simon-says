import { createSelector } from 'reselect';

const selectPads = (state) => state.pads;

export const selectActivePad = createSelector(
  (state) => state.pads,
  (pads) => pads.find(({ active }) => active)
);

export default selectPads;
