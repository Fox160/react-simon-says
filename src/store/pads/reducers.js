const reducers = {
  lightenPad: (state, action) => {
    const { payload } = action;

    return state.map((pad) => ({
      ...pad,
      active: payload === pad.id,
    }));
  },
  lightenOffPad: (state, action) => {
    return state.map((pad) => ({
      ...pad,
      active: false,
    }));
  },
};

export default reducers;
