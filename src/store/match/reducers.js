import getRandomId from '../../utils/getRandomId';

const reducers = {
  startGame: (state, { payload }) => {
    state.gameOver = false;
    state.guessed = [];
    state.all = state.all.concat(payload.next);
  },
  nextLevel: (state) => {
    const next = getRandomId();

    state.guessed = [];
    state.all = state.all.concat(next);
  },
  guessColor: (state, { payload }) => {
    state.guessed = payload.succeeded
      ? state.guessed.concat(payload.id)
      : state.guessed;
  },
  clearGame: (state) => {
    state.guessed = [];
    state.all = [];
  },
  endGame: (state) => {
    state.gameOver = true;
  },
};

export default reducers;
