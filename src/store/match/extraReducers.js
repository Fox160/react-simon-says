import { createAsyncThunk } from '@reduxjs/toolkit';
import { lightenOffPad, lightenPad } from '../pads/slice';
import sleep from '../../utils/sleep';
import { selectLevelSpeed } from '../levels/selectors';
import { guessColor, startGame as startMatch } from './slice';
import getRandomId from '../../utils/getRandomId';

export const startGame = createAsyncThunk(
  'match/start',
  async (_, { dispatch }) => {
    try {
      dispatch(startMatch({ next: getRandomId() }));
    } catch (e) {
      console.error(e);
    }
  }
);

export const sing = createAsyncThunk(
  'match/sing',
  async (_, { dispatch, getState }) => {
    try {
      const match = getState().match;
      const levelSpeed = selectLevelSpeed(getState());

      for (const id of match.all) {
        dispatch(lightenPad(id));
        await sleep(levelSpeed * 1000);
        dispatch(lightenOffPad());
        await sleep(levelSpeed * 1000);
      }

    } catch (e) {
      console.error(e);
    }
  }
);

export const guess = createAsyncThunk(
  'match/guess',
  async ({ succeeded, id }, { dispatch, getState }) => {
    try {
      const reducedDelayTime = 400;

      dispatch(guessColor({ succeeded, id }));
      dispatch(lightenPad(id));
      await sleep(reducedDelayTime);
      dispatch(lightenOffPad());
      await sleep(reducedDelayTime);

      const { all, guessed } = getState().match;
      const done = all.length === guessed.length && succeeded;

      return new Promise((r) => r({ done }));
    } catch (e) {
      console.error(e);
    }
  }
);

const extraReducers = (builder) => {
  builder.addCase(sing.fulfilled, (state) => ({ ...state }));
  builder.addCase(guess.fulfilled, (state) => ({ ...state }));
  builder.addCase(startGame.fulfilled, (state) => ({ ...state }));
};

export default extraReducers;
