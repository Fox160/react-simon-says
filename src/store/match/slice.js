import { createSlice } from '@reduxjs/toolkit';
import reducers from './reducers';
import extraReducers from './extraReducers';

const initialState = {
  guessed: [],
  all: [],
  gameOver: false,
};

const { actions, reducer } = createSlice({
  name: 'match',
  initialState,
  reducers,
  extraReducers,
});

export const { startGame, nextLevel, guessColor, clearGame, endGame } = actions;

export default reducer;
