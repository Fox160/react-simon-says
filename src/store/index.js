import { configureStore } from "@reduxjs/toolkit"
import logger from "redux-logger"
import levels from "./levels";
import match from './match';
import pads from "./pads";

const reducer = {
  levels,
  match,
  pads
}

const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});

export default store;
