import { createSlice } from '@reduxjs/toolkit';
import reducers from './reducers';

const initialState = {
  easy: { value: 1.5, active: true },
  medium: { value: 1.0, active: false },
  hard: { value: 0.4, active: false },
};

const { actions, reducer } = createSlice({
  name: 'levels',
  initialState,
  reducers,
});

export const { setLevel } = actions;

export default reducer;
