const reducers = {
  setLevel: (state, { payload }) => {
    Object.entries(state).forEach((item) => {
      const [name] = item;

      state[name]['active'] = payload === name;
    });
  },
};

export default reducers;
