const selectLevels = (state) => state.levels;

export const selectLevelSpeed = (state) => {
  const res = Object.values(state.levels).filter(
    (value) => value.active === true
  )[0];

  return res.value;
};

export default selectLevels;
