export const ids = [
  'green',
  'red',
  'yellow',
  'blue',
];

const getRandomId = () => ids[Math.floor(Math.random() * ids.length)];

export default getRandomId;
