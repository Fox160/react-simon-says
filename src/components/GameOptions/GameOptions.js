import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import selectLevels from '../../store/levels/selectors';
import { clearGame } from '../../store/match/slice';
import { setLevel } from '../../store/levels/slice';

const GameOptions = () => {
  const levels = useSelector(selectLevels);
  const dispatch = useDispatch();

  const handleChange = (e) => {
    const { value } = e.target;

    dispatch(clearGame());
    dispatch(setLevel(value));
  };

  if (!Object.keys(levels).length) {
    return null;
  }

  return (
    <div>
      Game Options:
      {Object.entries(levels).map(([name, level]) => (
        <div key={name}>
          <input
            type="radio"
            name="speed"
            value={name}
            id={name}
            checked={level.active}
            onChange={handleChange}
          />
          <label htmlFor={name}>{name}</label>
        </div>
      ))}
    </div>
  );
};

export default GameOptions;
