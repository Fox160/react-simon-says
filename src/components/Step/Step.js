import classNames from 'classnames';
import React from 'react';

import styles from './Step.module.scss';

const Step = ({ value, onClick, isActive, color }) => (
  <button
    style={{ backgroundColor: `${color}` }}
    type="button"
    data-value={value}
    onClick={onClick}
    className={classNames(styles.step, { [styles.active]: isActive })}
  />
);

export default Step;
