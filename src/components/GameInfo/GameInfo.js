import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { sing } from '../../store/match/extraReducers';
import { startGame } from '../../store/match/extraReducers';
import selectMatch from '../../store/match/selectors';
import { clearGame } from '../../store/match/slice';

import GameOptions from '../GameOptions/GameOptions';

import styles from './GameInfo.module.scss';

const GameInfo = () => {
  const dispatch = useDispatch();
  const { all, gameOver } = useSelector(selectMatch);

  const handleClick = () => {
    dispatch(clearGame());
    dispatch(startGame());
    dispatch(sing());
  };

  const infoText = gameOver ? 'You lose' : `Round: ${all.length}`;
  const btnText = gameOver ? 'Try again' : 'Start';

  return (
    <div className={styles.gameInfo}>
      <div className={styles.info}>
        {infoText}

        {(gameOver || all.length === 0) && (
          <button type="button" className={styles.btn} onClick={handleClick}>
            {btnText}
          </button>
        )}
      </div>
      <GameOptions />
    </div>
  );
};

export default GameInfo;
