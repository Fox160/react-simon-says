import React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

import { clearGame, nextLevel, endGame } from '../../store/match/slice';
import { guess, sing } from '../../store/match/extraReducers';

import Step from '../Step';

import styles from './Board.module.scss';

import sleep from '../../utils/sleep';

const Simon = () => {
  const dispatch = useDispatch();
  const { match, pads } = useSelector((state) => state, shallowEqual);

  const handleMouse = (e) => {
    const { value } = e.target.dataset;
    const tail = match.guessed.length;
    const succeeded = match.all[tail] === value;

    if (succeeded) {
      dispatch(guess({ id: value, succeeded })).then(async ({ payload }) => {
        const { done } = payload;

        if (done) {
          dispatch(nextLevel());
          await sleep(500);
          dispatch(sing());
        }
      });
    } else {
      dispatch(endGame());
      dispatch(clearGame());
    }
  };

  return (
    <div className={styles.simon}>
      {pads.map(({ id, active }) => (
        <Step
          key={id}
          color={id}
          value={id}
          isActive={active}
          {...(match.all.length > 0 && { onClick: handleMouse })}
        />
      ))}
    </div>
  );
};

export default Simon;
